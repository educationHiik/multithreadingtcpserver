/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package hiik.multithreadserver;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

/**
 *
 * @author vaganovdv
 */
public class TcpServer {

    // Класс, запукающий на выполнение ппотоки - ограничение  3 (потока или клиента)
    private Executor executor = Executors.newFixedThreadPool(3);
    private ServerSocket server;         // Серверный сокет для обслуживания клиентсикх сокетов
    private DataBaseServer dataBaseServer;
    private int serverTcpPort;
    

    /**
     * @param args the command line arguments
     */
    public void start() throws IOException {

        try {
            server = new ServerSocket(serverTcpPort); // создание сервера

            System.out.println("Сервер TCP стартовал");
            System.out.println("IP адрес сервера:  [" + server.getInetAddress().getHostAddress() + "]");
            System.out.println("Имя узла сервера:  [" + server.getInetAddress().getHostName() + "]");
            System.out.println("TCP порт сервера:  [" + serverTcpPort + "]");
            System.out.println("Ожидание подключения клиента...");
            System.out.println("");

            // Бесконечный цикл подключения клиентов 
            while (true) {
                Socket clientSocket = server.accept(); // Обработка подключения клиента и получение доступа 
                // к клиентскому сокету

                // Создание экземпляра класса для обработки нового клиента 
                ClientProcessor clientProcessor = new ClientProcessor();
                clientProcessor.setClientSocket(clientSocket);
                clientProcessor.setDataBaseServer(dataBaseServer);

                executor.execute(clientProcessor);
            }

        } catch (IOException e) {
            System.out.println("Ошибка ввода-вывода сетевого соединения");
            System.out.println("Описание ошибки: {" + e.getLocalizedMessage() + "}");

        } finally {
            System.out.println("");
            System.out.println("SERVER: Завершение работы сервера...");

            if (server != null) {
                server.close();
            }

            System.out.println("SERVER: Работа сервера завершена");

        }
    }

    public void setDataBaseServer(DataBaseServer dataBaseServer) {
        this.dataBaseServer = dataBaseServer;
    }


    public void setServerTcpPort(int serverTcpPort) {
        this.serverTcpPort = serverTcpPort;
    }

    
    
}
