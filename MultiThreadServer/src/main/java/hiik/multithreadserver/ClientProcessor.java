/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package hiik.multithreadserver;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author vaganovdv
 */
class ClientProcessor implements Runnable {

    // Экзмепляры получаемые для взаимодействия с клиентов
    private InputStream in;             // Поток чтения из сокета
    private OutputStream out;            // Поток записи в сокет  
    private Socket clientSocket;             // Клиентский сокет  
    private DataBaseServer dataBaseServer;
    
    private int clientPort;
    private String clientAddress;
    private String clientHost;
    private Scanner sc;
    private String hellow = "Сервер подключен";

    public void setIn(InputStream in) {
        this.in = in;
    }

    public void setOut(OutputStream out) {
        this.out = out;
    }

    public void setClientSocket(Socket clientSocket) {
        this.clientSocket = clientSocket;
    }

    public void setDataBaseServer(DataBaseServer dataBaseServer) {
        this.dataBaseServer = dataBaseServer;
    }
    
    

    @Override
    public void run() {

        try {

            in = clientSocket.getInputStream();
            out = clientSocket.getOutputStream();

            out.write(hellow.getBytes());   // Заполнение  буфера отправки
            out.write("\n".getBytes());     //
            out.flush();
            sc = new Scanner(in);  // Скарование строк от клиента 
            PrintWriter toClient = new PrintWriter(out);
            String msg = "";

            // Получение параметров подключения клиента
            clientPort = clientSocket.getPort();
            clientAddress = clientSocket.getLocalAddress().getHostAddress();
            clientHost = clientSocket.getLocalAddress().getCanonicalHostName();

            msg = String.format("==> Подключен клиент: %-20s %-20s %-7s", "[" + clientHost, clientAddress.toString(), "" + clientPort + "]");
            System.out.println(msg);

            // Цикл чтения строк от клиента
            while (sc.hasNextLine()) {
                String clientmsg = sc.nextLine();// Чтение строки от клиента

                // Форматирование строки для отображения параметров и тестового сообщения от клиента 
                //
                //                             host  ip   port      clientmsg
                //                              |     |    |          |
                msg = String.format("SERVER: %-15s %-15s %-7s %-5s %-120s", "[" + clientHost, clientAddress.toString(), "" + clientPort + "]", "==>", clientmsg);

                System.out.println(msg);          // Печать строки от клиента на сервере  
                toClient.println(msg);            // Отправка текста от клиента клиенту

                if (clientmsg.equalsIgnoreCase("exit")) {
                    String client = String.format("%-20s %-20s %-7s", "[" + clientHost, clientAddress.toString(), "" + clientPort + "]");
                    msg = "SERVER: Получена команда завершения работы от клиента " + client + "";
                    System.out.println(msg);
                    closeClientConnection(); 
                    break;
                }
                
                
                if (clientmsg.equalsIgnoreCase("list")) {
                    String client = String.format("%-20s %-20s %-7s", "[" + clientHost, clientAddress.toString(), "" + clientPort + "]");
                    msg = "SERVER: Получена команда запроса списка студентов " + client + "";
                     System.out.println(msg);
                    for (Student student : dataBaseServer.getStudentList()) {
                        
                        System.out.println(student);
                        out.write(student.toString().getBytes());
                        out.flush();
                    }
                }

            }
           // Завершение работы сервера

            // Отправка  частично зполненный буфер через сетевое соединение
        } catch (IOException ex) {
            Logger.getLogger(ClientProcessor.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void closeClientConnection() {
        System.out.println("SERVER: Закрытие клиентского соединения....");
        try {
            if (sc != null) {
                sc.close();
            }
            if (out != null) {
                out.close();
            }
            if (in != null) {
                in.close();
            }
            if (clientSocket != null && !clientSocket.isClosed()) {
                clientSocket.close();
            }

            String msg = String.format("==> Отключен  клиент:  %-20s %-20s %-7s", "[" + clientHost, clientAddress.toString(), "" + clientPort + "]");
            System.out.println(msg);
        } catch (IOException e) {
            System.out.println("SERVER: Ошибка сетевого соединения при завершении работы: {" + e.getLocalizedMessage() + "}");
        }

    }

}
