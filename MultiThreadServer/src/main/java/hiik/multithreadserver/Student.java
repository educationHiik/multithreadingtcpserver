/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package hiik.multithreadserver;

/**
 *
 * @author vaganovdv
 */
public class Student {

    private String fam;
    private String im;
    private String ot;
    private String cource;
    private String studentGroup;

    @Override
    public String toString() {
        return "Студент {" + "Фамилия=" + fam + ", Имя=" + im + ", Отчество=" + ot + ", Курс =" + cource + ", Группа=" + studentGroup + '}';
    }
    
    
    
    /**
     * @return the fam
     */
    public String getFam() {
        return fam;
    }

    /**
     * @param fam the fam to set
     */
    public void setFam(String fam) {
        this.fam = fam;
    }

    /**
     * @return the im
     */
    public String getIm() {
        return im;
    }

    /**
     * @param im the im to set
     */
    public void setIm(String im) {
        this.im = im;
    }

    /**
     * @return the ot
     */
    public String getOt() {
        return ot;
    }

    /**
     * @param ot the ot to set
     */
    public void setOt(String ot) {
        this.ot = ot;
    }

    /**
     * @return the cource
     */
    public String getCource() {
        return cource;
    }

    /**
     * @param cource the cource to set
     */
    public void setCource(String cource) {
        this.cource = cource;
    }

    /**
     * @return the studentGroup
     */
    public String getStudentGroup() {
        return studentGroup;
    }

    /**
     * @param studentGroup the studentGroup to set
     */
    public void setStudentGroup(String studentGroup) {
        this.studentGroup = studentGroup;
    }

   

}
