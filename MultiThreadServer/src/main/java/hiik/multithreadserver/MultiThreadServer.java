/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */

package hiik.multithreadserver;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author vaganovdv
 */
public class MultiThreadServer {

    public static void main(String[] args) {
        
        Student s1 = new Student();
        s1.setFam("Бельды");
        s1.setIm("Марк");
        s1.setOt("Николаевич");
        
        DataBaseServer dataBaseServer =new DataBaseServer();
        dataBaseServer.getStudentList().add(s1);
        
        
        
        TcpServer server = new TcpServer();
        server.setServerTcpPort(7777);
        server.setDataBaseServer(dataBaseServer);
        try {
            server.start();
        } catch (IOException ex) {
            Logger.getLogger(MultiThreadServer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
